var express = require('express');
var router = express.Router();
var models = require('../models');

var criteria = function(req) {
return { where: {id: req.params.id} };
}

/* GET videos listing. */
router.get('/', function(req, res, next) {
  res.format({
    json: function () {
      models.videos.findAll().then(videos => {
        res.json({videos:videos});
      });
    }
  });
});

/* Send the video to api by post*/
router.post('/', function(req, res, next) {
  var amqp = require('amqplib/callback_api');
	var video = models.videos.create({ url:req.param('url'),estado:"0" });
  var currentId=0;
  video.then(video => {
    console.log(video.id);
    currentId = video.id;
  });
  amqp.connect('amqp://localhost', function(err, conn) {
    conn.createChannel(function(err, ch) {
      var videoinfo ={
      "url":req.param('url'),
      "tipo":req.param('feature'),
      "cantidad":req.param('cantidad'),
      "id":currentId
      }
      var videoInfoString = JSON.stringify(videoinfo);
      var q = 'task_queue';
      var msg = process.argv.slice(2).join(' ') || videoInfoString;

      ch.assertQueue(q, {durable: true});
      ch.sendToQueue(q, new Buffer(msg), {persistent: true});
    });

  });
  res.format({
    json: function () {
      video.then(video => {

        res.json(video);
      });
    }
  });

});

router.get('/:id', function(req, res, next) {
  var video = models.videos.findOne(criteria(req));
  res.format({
    json: function () {
      video.then(video => {

        res.json(video);
      });
    }
  });

});
module.exports = router;
